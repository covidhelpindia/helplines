---
title: "Central list "
weight: 2
---
{{< block "mt-1 mb-2" >}}

{{< column >}}
### Additional central resources

#### Medicines Distributor

- [Remdesivir Distributor List](https://drive.google.com/file/d/1UL9N1Lg-6qokjgRg0oRWQgR2RNuH8nZG/view?usp=sharing)
- [Remdesivir Distributor List 2](https://docs.google.com/spreadsheets/d/1J-m84rr-tV47wZrUKn41jlTTID5FNcMs5MwyYHt9fLU/htmlview)
- [Tocilizumab Distributors list](https://drive.google.com/file/d/1TGMe3uoEVKHZa9dc9c3n4wddIOjSXUe2/view?usp=sharing)

#### Plasma Donors

- www.dhoondh.com
- www.plasmadonor.in
- www.needplasma.in
- www.plasmaline.in

#### Volunteers / Help groups

#### Support group / Mental health

{{< /column >}}
{{< /block >}}

## Union Territories 
{{< block "mt-1 mb-2" >}}


{{< column >}}
### Andaman and Nicobar Islands
 - 03192-232102
### Chandigarh 

* Helpline
  - 9779558282
* Tiffin
  - 9247756006
* Oxygen
  - Hitech Industries Limited - 9878800060 / 9872100006 
### Dadra and Nagar Haveli and Daman & Diu 
- 104
### Jammu & Kashmir 
- 01912520982, 0194-2440283
### Ladakh 
- 01982256462
### Lakshadweep 
- 104
### Puducherry 
- 104
### Delhi
- 011-22307145

#### Medicines Distributor

#### Volunteers / Help groups

#### Support group / Mental health
{{< /column >}}
{{< /block >}}