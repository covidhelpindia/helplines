+++
title="Bihar"
+++
### Main Helplines
- 104
#### Tiffin Services

#### Medicines / Oxygen Distributor

* Patna

  - Oxygen

    |Name      |PhoneNumber|
    |----------|-----------|
    |Vidya     |9835420734 |
    |Sanjay    |8809989999 |
    |Amresh    |8797348700 |
    |Oxygen Bank|9334206111 |
    |Ramesh    |9334486903 |
    |Ajay      |7257804224 |
    |Ronit     |9015780787 |
    |TSingh    |9934716122 |
    |Santosh   |9835287776 |
    |Vishwanath|995599981  |
    |Marwari Sammelan|933446039  |
    |SP Verma   |9534771419 |
    |Gaurav    |9308409095 |


#### Volunteers / Help groups

#### Support group / Mental health

