+++
title="Karnataka"
+++
#### Main Helplines
- 104
- 8951755722 (GoK)
#### Tiffin Services
  
 * Bengaluru 
   
|Name                              |Location                                                                 |Phone           |
|-------------------------------------------|----------------------------------------------------------------------------|---------------------|
|Hansika Subramanian                        |JP Nagar, Jayanagar, Banashankari, BTM layouts                              |9167347028           |
|Ankit Vengurlekar                          |HSR Layout/Koramangala/Bellandur                                            |8356809393           |
|Sneha Vachhaney                            |Bellandur, Sarjapur, Iblur, Koramangala, HSR, Yamlur, Indiranagar           |8884442726           |
|                                           |                                                                            |                     |
|Swaad Caterers |(not sure)                                                                  |9731794455 9538874717|
|Anucookingstory                            |CV Raman Nagar, Kaggadaspura, Indiranagar, HAL, Tippasandra                 |8792734027           |
|Aparna Juyal                               |Akshayanagar, Begur, Bommanahalli, Hulimavu, Bilekahalli                    |9885022255           |
|Malini Balachandran                        |Nearby Hoodi, Mahadevapura, ITPL                                            |9845190904           |
|Monika Manchanda                           |Doddenkundi, Mahadevpura, Whitefield, ORR, CV Raman Nagar                   |9845497789           |
|Rushitha Samavedam                         |Kumara Park West, Seshadaripuram, Malleswaram, Sadashiv Nagar, Vasanth Nagar|9538207070           |
|Samhitha Bhat                              |Balaji Elegance, Prashant Layout, Whitefield Bangalore                      |7259454481           |
|Basreena                                   |4th block Koramangala                                                       |9036685450           |
|Krithika                                   |JP Nagar, Jayanagar                                                         |8792461933           |
|Nivedita                                   |Koramangala / HSR Layout                                                    |9354400993           |
|Anushree                                   |RT Nagar/ Sadashivnagar/Malleswaram/Ganganagar                              |9538799132           |
|Madhubala                                  |HSR Layout/ Bellandur/Bommanhali/ Koramngala                                |7708951893           |
 
      

#### Medicines Distributor

#### Volunteers / Help groups

#### Support group / Mental health

