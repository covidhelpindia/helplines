---
title: "State list "
weight: 3
---

{{< block "mb-2" >}}
{{< column >}}
### State List and some import numbers

- [Andhra Pradesh](./andhra)
- [Arunachal Pradesh](./arunachal)
- [Assam](./assam)
- [Bihar](./bihar)
- [Chhattisgarh](./cgh)
- [Goa](./goa)
- [Gujarat](./gujarat)
- [Haryana](./haryana)
- [Himachal Pradesh](./himachal)
- [Jharkhand](./jkd)
- [Karnataka](./karnataka)
- [Kerala](./kerala)
- [Madhya Pradesh](./mp)
- [Maharashtra](./maha)
- [Manipur](./manipur)
- [Meghalaya](./meg)
- [Mizoram](./mizo)
- [Nagaland](./nagaland)
- [Odisha](./ods)
- [Punjab](./punjab)
- [Rajasthan](./raj)
- [Sikkim](./sikkim)
- [Tamil Nadu](./tn)
- [Telangana](./tgn)
- [Tripura](./tripura)
- [Uttar Pradesh](./up)
- [Uttarakhand](./ukd)
- [West Bengal](./wb)


{{< /column >}}
{{< /block >}}