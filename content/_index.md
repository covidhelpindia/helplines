+++
title = "COVID-19 Helplines"
# define chart data here
[data]
  fileLink = "content/projects.csv" # path to where csv is stored
  colors = ["#627c62", "#11819b", "#ef7f1a", "#4e1154"] # chart colors
  columnTitles = ["Section", "Status", "Author"] # optional if no table will be displayed from dataset
  baseChartOn = 3 # number of column the chart(s) and graph should be drawn from # can be overridden directly via shortcode parameter # it's therefore optional
  title = "Projects"
+++

# Directory of national and statewise helplines
{{< block "grid-3" >}}
{{< column >}}




## GoI websites
- [Ministry of Health and Family welfare (MOHFW)](https://www.mohfw.gov.in)
- [MyGov Covid Dashboard](https://www.mygov.in/covid-19)
- [Vaccination FAQ on MOFHW](https://www.mohfw.gov.in/covid_vaccination/vaccination/index.html)
- [Inter Ministerial Notifications](https://covid19.india.gov.in)

## Central Helplines-MHA
- +91-11-23978046
- 1075
- ncov2019@gov.in
- 1930 / 1944 (For NE)

{{< button "docs/" "Complete List" >}}
{{< /column >}}

{{< column >}}
## Other Websites

- [Zoho Covid Dashboard](https://www.zoho.com/covid/)
- [Covid19 India](https://www.covid19india.org)

{{< /column >}}

{{< column >}}
![diy](images/hands-heart.jpg)
{{< /column >}}


{{< /block >}}

{{< tip >}}
Feel free to open a [PR](https://gitlab.com/covidehelpindia/helplines/pulls), to add or remove data.
{{< /tip >}}